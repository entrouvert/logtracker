import pytest
import django_webtest

from django.contrib.auth.models import User
from django.test.utils import override_settings


@pytest.fixture
def auth(db, client):
    User.objects.create_user(username='john', password='Doe')
    client.login(username='john', password='Doe')


@pytest.fixture
def app(request):
    wtm = django_webtest.WebTestMixin()
    wtm._patch_settings()
    request.addfinalizer(wtm._unpatch_settings)
    return django_webtest.DjangoTestApp()


@pytest.fixture
def logged_app(db, app, username='admin', password='password'):
    User.objects.create_superuser('admin', email=None, password='password')
    login_page = app.get('/admin/login/')
    login_form = login_page.forms[0]
    login_form['username'] = username
    login_form['password'] = password
    resp = login_form.submit()
    assert resp.status_int == 302
    return app


@pytest.fixture
@override_settings(DEBUG=True)
def data(db, app):
    stream = open('tests/example.journal_export', 'rb').read()
    app.post('/upload', stream)
