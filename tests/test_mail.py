import json

from logtracker.mail.utils import extract_exim_emails
from logtracker.mail.models import Mail
from logtracker.journal.models import Entry


def test_mail_api(auth, client):
    page = client.get('/api/mail/')
    assert page.status_code == 200


def test_entries_list(auth, client):
    page = client.get('/api/mail/')
    assert page.status_code == 200


def test_mails(logged_app, data):
    extract_exim_emails()
    assert len(Mail.objects.all()) == 129

    page = logged_app.get('/api/mail/')
    assert page.status_code == 200
    content = json.loads(page.content.decode('utf-8'))
    #print(json.dumps(content, indent=2))
