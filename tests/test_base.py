import pytest
import json
import time

from logtracker.journal.models import Entry


def test_auth(client, monkeypatch):
    resp = client.get('/api/mail/')
    assert resp.status_code == 302


def test_home_page(client, auth):
    page = client.get('/')
    assert page.status_code == 200


def test_entries_list(auth, client, data):
    page = client.get('/api/journal/')
    assert page.status_code == 200

    journal_entries = Entry.objects.all()
    assert len(journal_entries) == 1030

    entries = json.loads(page.content.decode())
    assert len(entries) == 1030
