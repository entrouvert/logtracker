ALLOWED_HOSTS = ['*']

INSTALLED_APPS += [
    'mellon'
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'USER': 'logtracker',
        'NAME': 'logtracker',
    }
}

TIME_ZONE = 'Europe/Paris'

AUTHENTICATION_BACKENDS = ( 'django.contrib.auth.backends.ModelBackend',
        'mellon.backends.SAMLBackend')

LOGIN_URL = 'mellon_login'
LOGOUT_URL = 'mellon_logout'

MELLON_ATTRIBUTE_MAPPING = {
        'username': '{attributes[username][0]}',
        'email': '{attributes[email][0]}',
        'first_name': '{attributes[first_name][0]}',
        'last_name': '{attributes[last_name][0]}',
        }


MELLON_SUPERUSER_MAPPING = {'is_superuser': (u'true',)}
MELLON_USERNAME_TEMPLATE = '{attributes[username][0]}'

MELLON_PUBLIC_KEYS = ['/etc/logtracker/saml.crt']
MELLON_PRIVATE_KEY = '/etc/logtracker/saml.key'
MELLON_IDENTITY_PROVIDERS = [
                {'METADATA': '/etc/logtracker/idp-metadata.xml',},
                ]
