#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import subprocess
import shutil
import sys

from setuptools.command.install_lib import install_lib as _install_lib
from distutils.command.build import build as _build
from setuptools.command.sdist import sdist
from distutils.cmd import Command
from setuptools import setup, find_packages


class deb(sdist):
    def run(self, *args):
        version = get_version()
        orig_version = '0.1'
        sdist.run(self)
        shutil.move("dist/logtracker-%s.tar.gz" % version, '../logtracker_%s.orig.tar.gz' % orig_version)
        os.system('dpkg-source -b .')
        os.system('sudo HOME=$HOME DIST=buster cowbuilder --build ../logtracker_%s-1.dsc' % orig_version)


class eo_sdist(sdist):
    def run(self):
        if os.path.exists('VERSION'):
            os.remove('VERSION')
        version = get_version()
        version_file = open('VERSION', 'w')
        version_file.write(version)
        version_file.close()
        sdist.run(self)
        if os.path.exists('VERSION'):
            os.remove('VERSION')


def get_version():
    '''Use the VERSION, if absent generates a version with git describe, if not
       tag exists, take 0.0- and add the length of the commit log.
    '''
    if os.path.exists('VERSION'):
        with open('VERSION', 'r') as v:
            return v.read()
    if os.path.exists('.git'):
        p = subprocess.Popen(['git','describe','--dirty=.dirty','--match=v*'],
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        result = p.communicate()[0]
        if p.returncode == 0:
            result = result.decode('ascii').strip()[1:] # strip spaces/newlines and initial v
            if '-' in result: # not a tagged version
                real_number, commit_count, commit_hash = result.split('-', 2)
                version = '%s.post%s+%s' % (real_number, commit_count, commit_hash)
            else:
                version = result
            return version
        else:
            return '0.0.post%s' % len(
                    subprocess.check_output(
                            ['git', 'rev-list', 'HEAD']).splitlines())
    return '0.0'


class compile_translations(Command):
    description = 'compile message catalogs to MO files via django compilemessages'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        try:
            from django.core.management import call_command
            for path, dirs, files in os.walk('fargo'):
                if 'locale' not in dirs:
                    continue
                curdir = os.getcwd()
                os.chdir(os.path.realpath(path))
                call_command('compilemessages')
                os.chdir(curdir)
        except ImportError:
            sys.stderr.write('!!! Please install Django >= 1.4 to build translations\n')


class build(_build):
    sub_commands = [('compile_translations', None)] + _build.sub_commands


class install_lib(_install_lib):
    def run(self):
        self.run_command('compile_translations')
        _install_lib.run(self)


setup(
    name='logtracker',
    version=get_version(),
    description='Exim Logger',
    long_description=open('README').read(),
    author="Entr'ouvert",
    author_email='info@entrouvert.com',
    packages=find_packages(),
    include_package_data=True,
    scripts=('manage.py',),
    url='https://dev.entrouvert.org/projects/logtracker/',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
    ],
    install_requires=[
        'django>=1.11',
        'psycopg2',
        'django-mellon'
    ],
    zip_safe=False,
    cmdclass={
        'build': build,
        'compile_translations': compile_translations,
        'install_lib': install_lib,
        'sdist': eo_sdist,
        'deb': deb,
    },
)
