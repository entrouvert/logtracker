from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings

from logtracker.journal.views import APIEntriesList, EntriesList, HomeView, UploadView
from logtracker.mail.views import EmailsList, SendersList, MailHome

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^journal/$', EntriesList.as_view(), name='journal'),
    url(r'^api/journal/$', APIEntriesList.as_view(), name='journal-api'),
    url(r'^api/mail/$', EmailsList.as_view(), name='mail-api'),
    url(r'^mail/senders/$', SendersList.as_view(), name='senders'),
    url(r'^upload$', UploadView, name='upload'),
]

if 'mellon' in settings.INSTALLED_APPS:
    urlpatterns += url(r'^accounts/mellon/', include('mellon.urls')),
