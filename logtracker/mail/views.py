from urllib.parse import unquote
from django.views.generic import TemplateView
from django.views.generic.list import ListView
from django.http import JsonResponse, HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse
from logtracker.mail.models import Mail, Sender, Recipient


class EmailsList(LoginRequiredMixin, ListView):
    def get_queryset(self):
        params = self.request.GET
        return Mail.objects.dump(host=params.get('host'), sender=params.get('sender'), lines=params.get('lines'))

    def get(self, request, *args, **kwargs):
        data = {m.identifier: [e.data.get('MESSAGE') for e in m.entries_list] for m in self.get_queryset()}
        return JsonResponse({'data': data})


class MailHome(LoginRequiredMixin, TemplateView):
    template_name = 'mail/home.html'
    form_class = Recipient

    def post(self, request, *args, **kwargs):
        url = '%s?%s' % (reverse('mail-api'), '%s=%s' % (request.POST['field'], request.POST['address']))
        return HttpResponseRedirect(url)


class SendersList(LoginRequiredMixin, ListView):

    model = Sender
