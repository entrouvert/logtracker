from django.db import models
from django.contrib.postgres.fields import JSONField
from logtracker.journal.models import Entry
import json

TYPES = [('<=', 'Arrival'),
         ('=>', 'Delivery'),
         ('==', 'Defered'),
         ('**', 'Failed'),
         ('->', 'Additional'),
         ('*>', 'Suppressed'),
         ('Completed', 'Completed'),
         ('Frozen', 'Frozen'),
         ('SMTP error', 'SMTP error'),
         ]


class Property(models.Model):
    name = models.CharField(max_length=128, unique=True, db_index=True)
    data = JSONField(default=dict)


class Address(models.Model):
    email = models.EmailField(blank=True, null=True, db_index=True)

    class Meta:
        abstract = True
        ordering = ['email']

    def email_count(self):
        return self.mail_set.all().count()

    def pending_count(self):
        return self.mail_set.filter(has_completed=False).count()

    def error_count(self):
        return self.mail_set.filter(has_error=True).count()

    def __str__(self):
        return self.email


class Sender(Address):
    pass


class Recipient(Address):
    pass


class MailManager(models.Manager):
    def dump(self, host=None, lines=None, sender=None):
        qs = Mail.objects.all()
        if host:
            qs = qs.filter(host=host)
        if sender:
            qs = qs.filter(sender__email=sender)
        if lines:
            qs = qs.order_by('-timestamp')[:int(lines)][::-1]
        return qs


class Mail(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
    host = models.CharField(max_length=128, db_index=True)
    identifier = models.CharField(max_length=30, unique=True, db_index=True)
    sender = models.ForeignKey(Sender, null=True)
    recipients = models.ManyToManyField(Recipient)
    has_error = models.BooleanField(default=False, db_index=True)
    has_completed = models.BooleanField(default=False, db_index=True)
    entries = JSONField(default=list)
    objects = MailManager()

    def __str__(self):
        return '%s %s' % (self.identifier, self.sender)

    @property
    def entries_list(self):
        output = []
        for pk in self.entries:
            try:
                output.append(Entry.objects.get(pk=pk))
            except Entry.DoesNotExist:
                pass
        return output
