from django.core.management.base import BaseCommand, CommandError
from logtracker.data.models import Mail

class Command(BaseCommand):
    help = 'Collect entries from exim4 log'

    def add_arguments(self, parser):
        parser.add_argument('sender', type=str)

    def handle(self, *args, **options):
        for entry in Mail.objects.filter(sender=options['sender']):
            print(entry)
