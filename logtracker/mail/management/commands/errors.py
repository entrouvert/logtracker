from django.core.management.base import BaseCommand, CommandError
from logtracker.data.models import Mail

class Command(BaseCommand):
    help = 'Collect entries from exim4 log'

    def handle(self, *args, **options):
        for mail in Mail.objects.filter(has_error=True):
            print(mail)
