import datetime
from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

from logtracker.data.models import Mail

class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('-d', '--days')

    def handle(self, *args, **options):
        if options.get('days'):
            days = options.get('days')
        else:
            days = 3
        delay = timezone.now() - datetime.timedelta(days=days)
        Mail.objects.filter(stamp__lt=delay).delete()
