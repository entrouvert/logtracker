from django.core.management.base import BaseCommand, CommandError
from logtracker.mail.utils import extract_exim_emails

class Command(BaseCommand):
    help = 'Collect exim entries from journal entries'

    def handle(self, *args, **options):
        extract_exim_emails()
