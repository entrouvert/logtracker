from django.core.management.base import BaseCommand, CommandError
from logtracker.data.models import Sender

class Command(BaseCommand):
    help = 'Collect entries from exim4 log'

    def handle(self, *args, **options):
        for sender in Sender.objects.all():
            print("%s %s" % (sender, sender.emails_count))
