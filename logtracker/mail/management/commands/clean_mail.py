import datetime

from django.core.management.base import BaseCommand
from django.conf import settings

from logtracker.mail.models import Mail


class Command(BaseCommand):
    help = "Remove old mail entries"

    def add_arguments(self, parser):
        parser.add_argument("--keep", type=int, default=settings.MAIL_HISTORY)

    def handle(self, *args, **options):
        limit = datetime.datetime.now() - datetime.timedelta(days=options['keep'])
        Mail.objects.filter(timestamp__lt=limit).delete()
