import os
import sys
import fcntl
from contextlib import contextmanager
from io import BlockingIOError

@contextmanager
def lock(lockfile):
    try:
        handle = open(lockfile, 'w')
        fcntl.lockf(handle, fcntl.LOCK_EX | fcntl.LOCK_NB)
        yield
    except BlockingIOError:
        print('locked')
        sys.exit(1)
