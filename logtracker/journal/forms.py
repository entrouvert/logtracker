import datetime
from django import forms
from django.conf import settings
from logtracker.journal.models import Entry


class EntriesForm(forms.Form):
    host = forms.MultipleChoiceField(required=False)
    tenant = forms.MultipleChoiceField(required=False)
    _systemd_unit = forms.MultipleChoiceField(required=False)
    priority = forms.ChoiceField(required=False, choices=reversed([(i, i) for i in range(1, 8)]))
    since = forms.DateTimeField(required=False, initial=datetime.datetime.now() - datetime.timedelta(days=settings.LOGTRACKER_HISTORY))
    tail = forms.BooleanField(required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['host'].choices = set([(e, e) for e in Entry.objects.hosts()])
        self.fields['_systemd_unit'].choices = [(e, e) for e in Entry.objects.units()]
        self.fields['tenant'].choices = [(e, e) for e in Entry.objects.tenants()]
