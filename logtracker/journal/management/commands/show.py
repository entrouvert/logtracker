from django.core.management.base import BaseCommand, CommandError
from logtracker.journal.models import Entry
from pydoc import pager
import os
import textwrap


def bold(txt):
    bold = "\033[1m"
    end = "\033[0m"
    return bold + txt + end


class Command(BaseCommand):
    help = "Print entries"

    def add_arguments(self, parser):
        parser.add_argument("--full", action="store_true")
        parser.add_argument("--since")
        parser.add_argument("-n", "--lines", help="tail n lines")
        parser.add_argument("-p", "--priority")
        parser.add_argument("--no-pager", action="store_true")
        parser.add_argument('options', nargs="*", help="any systemd selector k=v")

    def handle(self, *args, **options):
        if options['no_pager'] or options['lines']:
            for l in self.get_lines(**options):
                print(l)
        else:
            pager('\n'.join([l for l in self.get_lines(**options)]))

    def get_lines(self, **options):
        _, columns = os.popen("stty size", "r").read().split()
        kwargs = {k: v for (k, v) in [o.split('=') for o in options['options']]}
        for entry in Entry.objects.extract(lines=options['lines'], since=options['since'], priority=options['priority'], **kwargs):
            line = "%s %s %s %s" % (
                entry.timestamp.astimezone().strftime("%b %d %X"),
                entry.host, entry.unit,
                entry.data.get("MESSAGE"),
            )
            priority = entry.data.get("PRIORITY")
            if not options.get("full"):
                line = textwrap.shorten(line, int(columns))
            if priority and int(priority) < 4:
                line = bold(line)
            yield line
