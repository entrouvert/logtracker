from django.core.management.base import BaseCommand, CommandError
from logtracker.journal.models import Entry
import os
import textwrap
from collections import Counter


def bold(txt):
    bold = "\033[1m"
    end = "\033[0m"
    return bold + txt + end


class Command(BaseCommand):
    help = "Print entries"

    def add_arguments(self, parser):
        parser.add_argument("--full", action="store_true")
        parser.add_argument("-n", "--lines", default=10)

    def handle(self, *args, **options):
        _, columns = os.popen("stty size", "r").read().split()
        messages = [entry.data.get("MESSAGE") for entry in Entry.objects.all()]
        for k, v in sorted(Counter(messages).items(), key=lambda i: i[1], reverse=True)[:options["lines"]]:
            print(v, k)
