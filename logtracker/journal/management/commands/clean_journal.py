import datetime

from django.core.management.base import BaseCommand
from django.conf import settings

from logtracker.journal.models import Entry


class Command(BaseCommand):
    help = "Remove old entries"

    def add_arguments(self, parser):
        parser.add_argument("--keep", type=int, default=settings.LOGTRACKER_HISTORY)

    def handle(self, *args, **options):
        limit = datetime.datetime.now() - datetime.timedelta(days=options['keep'])
        Entry.objects.filter(timestamp__lt=limit).delete()
